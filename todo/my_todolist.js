let input = document.querySelector('#new_td');

// 입력창에 엔터키에 대한 이벤트 발생
input.addEventListener('keypress', function(e){
    if (e.key === 'Enter'){
        if (input.value === ""){
			alert("Please write to do");
        }else{
            eventHandler(e);
        }
    }
});

// Event Propagation 를 막기 위해 핸들러 함수를 추가해서 추가 event 발생을 제거 후 진행
function eventHandler(event){   
    event.preventDefault();
    writeTD(input.value);
};

// del 키로 리스트 삭제 기능

function deleteToDo(event){
    // $( obj.parentNode ).remove(); // jQuery 이용방법
    const btn = event.target;
    const li = btn.parentNode;
    // 서버 List 삭제
    $.post("https://searchyours.site/My_c/delete_td",
        {
        del_td:li.querySelector("span").innerText
        },
        function (data){
            if (data === false){
                alert("Fail to Delete");
                return false;
            }
        }
    );
    // 브라우저 List 삭제
    li.remove();
};

// 리스트 추가 함수
function writeTD(text){
    let list = document.getElementById("td_list");
    let li = document.createElement("li");
    let btn = document.createElement("button");
    btn.innerText = "Del";
    btn.addEventListener('click', deleteToDo);
    let span = document.createElement("span");
    span.classList.add("tdCondition");
    span.setAttribute("onclick","tdChange(event);");
    span.innerText = text;
    li.appendChild(span);
    li.appendChild(btn);
    list.appendChild(li);
    $.post("https://searchyours.site/My_c/create_td",
            {
            new_td:text, new_date:writeDate(currentDate) // writeDate(currentDate)는 myCalendar.js에서 가져온 값 
            },
            function (data){
                if (data !== "Saved!"){
                    alert("Fail to Update");
                    return false;
                }
            }
    );
    input.value = "";
};

// 페이지 로딩시, 서버에 저장된 리스트 불러오기
document.addEventListener("DOMContentLoaded", loadList());

function loadList () {
    const list = document.getElementById("td_list");
    list.innerHTML = "";  
    $.post("https://searchyours.site/My_c/load_td",
        { 
            request_date:writeDate(currentDate)
         },
        function (data)
        {
            list.innerHTML = data;            
        }
    );
}

// 리스트 클릭시, 컨디션 변화주기(Scheduled,Done)
function tdChange(event){
    const span = event.target;
    console.log(span);
    
    if (span.className === "tdDone"){
        // 서버에 Condition을 Scheduled로 update, tdDone 클래스 삭제
        $.post("https://searchyours.site/My_c/upateSchedule",
            { updateList:span.innerText },
            function (data)
            {
                span.classList.remove("tdDone");           
                span.classList.add("tdScheduled");           
            }
        );
    }else{
        // 서버에 Condition을 done로 update, tdDone 클래스 추가
        $.post("https://searchyours.site/My_c/upateDone",
            { updateList:span.innerText },
            function (data)
            {
                span.classList.remove("tdScheduled");
                span.classList.add("tdDone");
            }
        );
    }
};