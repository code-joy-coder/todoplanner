// 현재 일자 정보 출력
let currentDate = new Date();
let writeDate = date => {
    return date.getFullYear() + "-" + (date.getMonth()+1) + "-" + date.getDate();
};

let printDate = date =>{
  if (currentDate.getDate() < 10){
    return `0${currentDate.getDate()}`;
  }else{
    return currentDate.getDate();
  }
};

let printDay = day => {
  switch (currentDate.getDay()){
    case 0 : return `Sunday`;
    case 1 : return `Monday`;
    case 2 : return `Tuesday`;
    case 3 : return `Wednesday`;
    case 4 : return `Thursday`;
    case 5 : return `Friday`;
    case 6 : return `Saturday`;
  }
}

let printMonth = month =>{
  switch (currentDate.getMonth()){
    case 0 : return `January`;
    case 1 : return `February`;
    case 2 : return `March`;
    case 3 : return `April`;
    case 4 : return `May`;
    case 5 : return `June`;
    case 6 : return `July`;
    case 7 : return `August`;
    case 8 : return `September`;
    case 9 : return `October`;
    case 10 : return `November`;
    case 11 : return `December`;
  }
}

// 몇 주차(week)인지 체크하는 함수로... 구글링으로 가져와서 가공함
let printWeek = week => {
    const firstDayOfYear = new Date(currentDate.getFullYear(), 0, 1);
    const pastDaysOfYear = (currentDate - firstDayOfYear) / 86400000;
    const howManyWeek = Math.ceil((pastDaysOfYear + firstDayOfYear.getDay() + 1) / 7);
    if (howManyWeek < 10) {
        return `Week 0${howManyWeek}`
    }else {
        return `Week ${howManyWeek}`
    };
}

function printCurrent(){
    document.getElementById('date_screen').innerText = printDate();
    document.getElementById('Day_screen').innerText = printDay();
    document.getElementById('Month_screen').innerText = printMonth();
    document.getElementById('Week_screen').innerText = printWeek();
    let checkDate = new Date();
    document.getElementById('today').innerText = ((writeDate(checkDate) === writeDate(currentDate)) ? "Today" : "");
};
printCurrent();


// 캘린더 부분 작업
let calendarContent = document.getElementById("calendar");
let first = new Date(currentDate.getFullYear(), currentDate.getMonth());
let last = new Date(currentDate.getFullYear(), currentDate.getMonth() + 1, 0);
let firstDay = first.getDay();
let lastDate = last.getDate();
let dayName = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
let calendarDay = 1;

function printCalendar() {
    // 캘린더 해더 부분( Month Year )
    let calendarHead = document.createElement("div");
    calendarHead.setAttribute("class", "calHead");
    calendarArrow1 = document.createElement("span");
    calendarArrow1.setAttribute("class", "prv");
    calendarArrow1.setAttribute("onclick", "prv_Month();");
    calendarArrow1.innerText = "<";
    calendarArrow2 = document.createElement("span");
    calendarArrow2.setAttribute("class", "next");
    calendarArrow2.setAttribute("onclick", "next_Month();");
    calendarArrow2.innerText = ">";
    calendarHead.innerText = `${printMonth()} ${currentDate.getFullYear()}`;
    calendarHead.appendChild(calendarArrow1);
    calendarHead.appendChild(calendarArrow2);
    calendarContent.appendChild(calendarHead);
    
    // 캘린더 요일 부분( Day )
    let calDay_tr = document.createElement("tr");
    for (let m = 0; m < 7; m++) {
        const calDay = document.createElement("td");
        calDay.setAttribute("class", dayName[m]);
        calDay.innerText = dayName[m];
        calDay_tr.appendChild(calDay);
    };
    calendarContent.appendChild(calDay_tr);

    for (let i = 0; i < 6; i++) {
        let tr = document.createElement("tr");
        for (let j = 0; j < 7; j++) {
            if ((i === 0 && j < firstDay) || calendarDay > lastDate) { // 월별 첫주,마지막주에 비어있는 요일 넘어가기
                const td = document.createElement("td");
                tr.appendChild(td);
            } else {
                const td = document.createElement("td");
                td.setAttribute("class", dayName[j]+" date");
                td.setAttribute("onclick", "changeDate(event)");
                td.innerText = calendarDay;
                tr.appendChild(td);
                calendarDay++
            }
        }
        calendarContent.appendChild(tr);
    };
}
printCalendar();

// 날짜 변경에 따른 변수 업데이트 함수
function updateCurrent(yy,mm,dd){
    currentDate = new Date(yy, mm, dd);
    first = new Date(yy, mm);
    last = new Date(yy, mm+1, 0);
    firstDay = first.getDay();
    lastDate = last.getDate();
}

// 캘린더 날짜 클릭에 따라 화면 전환
function changeDate(event){
    let targetDate = event.target;
    updateCurrent(currentDate.getFullYear(), currentDate.getMonth(), targetDate.innerText);
    printCurrent(); // 화면 날짜 변경 실행
    loadList (); // 해당 날짜의 리스트 불러오기
}

// 캘린더 월 이동
function prv_Month() {
    calendarContent.innerHTML="";
    updateCurrent(currentDate.getFullYear(), currentDate.getMonth()-1, currentDate.getDate());
    printCurrent();
    loadList ();
    calendarDay = 1;
    printCalendar();
};

function next_Month(){
    calendarContent.innerHTML="";
    updateCurrent(currentDate.getFullYear(), currentDate.getMonth()+1, currentDate.getDate());
    printCurrent();
    loadList ();
    calendarDay = 1;
    printCalendar();
};