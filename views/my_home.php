<? if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Manage Yours</title>
    <link rel="stylesheet" href="<?=base_url('src/todo/my_home.css')?>">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>

    <link rel="shortcut icon" href="#"> <!-- 파비콘(favicon) 에러 방지 -->

</head>
<body>
<div class="loader_bg"></div>
<div class="loader"></div>    

<div class="td_input"> 
    <div class="input_area">
        <nav class="Select">
            <button type="button" class="btn_td on">To Do</button>
            <button type="button" class="btn_schd">Schedule</button>
            <button type="button" class="btn_memo">Memo</button>
        </nav>
        <form class="content" onsubmit="return false;">
            <input class="new_td" id="new_td" name="new_td" placeholder="&nbsp;&nbsp;Add Your To Do" autofocus>
            <ul id="td_list">
            </ul>
        </form>
        <input type="button" class="btn_td" onclick="submit_td();" value="Update Your List">
        <a href="<?=base_url('My_c/todolist')?>" class="close">Close</a>
    </div>
</div>


<script src="<?=base_url('src/todo/my_home.js')?>"></script>
</body>
</html>