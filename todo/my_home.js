// 로딩 시, LOGO 나타났다가 사라지기
setTimeout(function(){
    $(".loader").fadeOut();
    $(".loader_bg").fadeOut();
    $(".td_input").fadeIn('slow'); 
},800);

// 서버로 전달할 오늘 날짜 정보 추출
let currentDate = new Date();
let writeDate = (date) => {
    return date.getFullYear() + "-" + (date.getMonth()+1) + "-" + date.getDate();
};

// 서버로 전달하는 ToDoList 폼전송 코드
function submit_td(){
    if ( toDoSaved.length != 0 ){     // 입력창의 리스트에 내용이 있는지 유무 검사
        for (let value of toDoSaved ){      // 저장된 값을 한개씩 빼서 for 문 실행
            // $.post("<?=base_url('My_c/create_td')?>",
            $.post("https://searchyours.site/My_c/create_td",
                    {
                    new_td:value, new_date:writeDate(currentDate) 
                    },
                    function (data){
                        if (data !== "Saved!"){
                            alert("Fail to Update");
                            return false;
                        }
                    }
            );
        }
        alert("Update is completed.");
        // window.location.href ="<?=base_url('My_c/todolist')?>";
        window.location.href ="https://searchyours.site/My_c/todolist";
        return false;
    }else{
        alert ("Please write your new list.");
    }
}

let input = document.querySelector('#new_td');

// 입력창에 엔터키에 대한 이벤트 발생
input.addEventListener('keypress', function(e){
    if (e.key === 'Enter'){
        if (input.value === ""){
			alert("Please write your list or click 'close' button to move to a list");
            return false;
        }else{
            eventHandler(e);
        }
    }
});

// Event Propagation 를 막기 위해 핸들러 함수를 추가해서 추가 event 발생을 제거 후 진행
function eventHandler(event){   
    event.preventDefault();
    writeTD(input.value);
};

// del 키로 리스트 삭제 기능
function deleteToDo(event){
    // $( obj.parentNode ).remove(); // jQuery 이용방법
    const btn = event.target;
    const li = btn.parentNode;
    const delList = li.querySelector('span').innerText;
    const delIndex = toDoSaved.indexOf(delList);
    toDoSaved.splice(delIndex,1);
    li.remove();
};

// 서버로 전송하기 위해 별도 저장
let toDoSaved = [];

function writeTD(text){
    // $("#td_list").append($("<li><span>" + text + "</span> <span class='deleteBtn' onclick='deleteToDo(this)'>Del</span></li>"));  // jQuery 이용방법    
    let list = document.getElementById("td_list");
    let liCount = list.childElementCount;
    
    if(liCount < 5){     // 리스트 입력 개수를 5개로 제한하는 제어문
        let li = document.createElement("li");
        let btn = document.createElement("button");
        btn.innerText = "Del";
        btn.addEventListener('click', deleteToDo);
        let span = document.createElement("span");
        span.innerText = text;
        li.appendChild(span);
        li.appendChild(btn);
        list.appendChild(li);
        input.value = "";
        toDoSaved.push(text);
    }else{
        alert ("Up to 5 lists can be updated at a time.");
        input.value = "";
    }  
};