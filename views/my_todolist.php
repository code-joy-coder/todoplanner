<? if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Manage Yours</title>
    <link rel="stylesheet" href="<?=base_url('src/todo/my_todolist.css')?>">
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>

    <link rel="shortcut icon" href="#"> <!-- 파비콘(favicon) 에러 방지 -->

</head>
<body>
<div class="td_Page">
    <header>
        <span id="date_screen"></span>
        <ul>
            <li id="Month_screen"></li>
            <li><span id="Day_screen"></span> | 
                <span id="Week_screen"></span></li>
            <li id="today">Today</li>
        </ul>
        <div id="calendar"></div>
    </header>

    <section class="listInput_area">
        <input class="new_td" id="new_td" name="new_td" placeholder="&nbsp;&nbsp;Add Your To Do" autofocus>        
    </section>

    <section class="content">
        <ul id="td_list">
        </ul>
    </section>
</div>

<script src="<?=base_url('src/todo/my_todolist_Calendar.js')?>"></script>
<script src="<?=base_url('src/todo/my_todolist.js')?>"></script>
</body>
</html>