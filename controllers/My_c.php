<? if(!defined('BASEPATH')) exit('No direct script access allowed');

    class My_c extends CI_Controller 
    {
        // 홈화면 view 이동
        public function home ()
        {
            $this->load->view('my_home');
        }
        // To Do List 페이지 view 이동
        public function todolist ()
        {
            $this->load->view('my_todolist');
        }
        // new List 생성
        public function create_td()
        {
            $this->load->model('My_m');
            $this->My_m->create_td($_POST["new_td"], $_POST["new_date"]);
                            
            echo "Saved!";
        }
        // 리스트 불러오기 
        public function load_td()
        {
            $this->load->model('My_m');

            // 완료되지 않은 리스트
            $result1 = $this->My_m->load_listScheduled($_POST["request_date"]);
            foreach ($result1->result_array() as $row ) {
                echo "<li><span class='tdScheduled' onclick='tdChange(event);'>".$row["List"]."</span><button onclick='deleteToDo(event)'>Del</button></li>";
            }
            // 완료된 리스트
            $result2 = $this->My_m->load_listDone($_POST["request_date"]);
            foreach ($result2->result_array() as $row ) {
                echo "<li><span class='tdDone' onclick='tdChange(event);'>".$row["List"]."</span><button onclick='deleteToDo(event)'>Del</button></li>";
            }
        }
        // 리스트 삭제
        public function delete_td()
        {
            $this->load->model('My_m');
            $this->My_m->delete_list($_POST["del_td"]);
        }
        // 리스트 업데이트 : done -> scheduled
        Public function upateSchedule()
        {
            $this->load->model('My_m');
            $this->My_m->upate_Schedule($_POST["updateList"]);
        }
        // 리스트 업데이트 : scheduled -> done
        Public function upateDone()
        {
            $this->load->model('My_m');
            $this->My_m->upate_Done($_POST["updateList"]);
        }
    }
?>