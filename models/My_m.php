<? if(!defined('BASEPATH')) exit('No direct script access allowed');

//application/models/My_m.php

	class My_m extends CI_Model 
	{
        // 리스트 생성
		public function create_td ($ToDo, $date)
		{
			$data = array(
                            'List' => $ToDo,
                            'Date' => $date,
                            'Condition' => 'Scheduled'
						);
				
            $this->db->insert('todo', $data);        
        }
        // 'Scheduled' 리스트 불러오기
        public function load_listScheduled ($date)
        {
            $this->db->where('Date', $date);
            $this->db->where('Condition', 'Scheduled');
            $result = $this->db->get('todo');
            return $result;
        }
        // 'Done' 리스트 불러오기
        public function load_listDone ($date)
        {
            $this->db->where('Date', $date);
            $this->db->where('Condition', 'Done');
            $result = $this->db->get('todo');
            return $result;
        }
        // 리스트 삭제
        public function delete_list ($list)
        {
            $this->db->where('List', $list);
            $this->db->delete('todo');
        }
        // 'Scheduled' 로 업데이트 
        public function upate_Schedule($list)
        {
            $data = array(
                'List' => $list,
                'Condition' => 'Scheduled'
            );
            $this->db->where('List', $list);
            $this->db->update('todo', $data);
        }
        // 'Done' 로 업데이트 
        public function upate_Done($list)
        {
            $data = array(
                'List' => $list,
                'Condition' => 'Done'
            );
            $this->db->where('List', $list);
            $this->db->update('todo', $data);
        }
	}
	
?>